/*
 * Copyright © 2020 traffxml.org.
 * 
 * This file is part of the traff-libmilestone library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lib.milestone;

import java.util.Objects;

import org.traffxml.traff.LatLon;

/**
 * Represents a motorway (or expressway) junction.
 */
public class Junction {
	/**
	 * The coordinates of the junction.
	 */
	public final LatLon geo;

	/**
	 * The reference number of the road on which the junction is located.
	 */
	public final String roadRef;

	/**
	 * The reference number of the junction, if any.
	 */
	public final String ref;

	/**
	 * The name of the junction.
	 */
	public final String name;

	/**
	 * The nearest kilometric point, often rounded to an integer (and frequently null).
	 */
	public final Distance distance;

	public Junction(String roadRef, String ref, String name, Distance distance, LatLon geo) {
		super();
		this.roadRef = roadRef;
		this.ref = ref;
		this.name = name;
		this.distance = distance;
		this.geo = geo;
	}

	public boolean equals(Junction that) {
		if (this == that)
			return true;
		if (that == null)
			return false;
		if (this.getClass() != that.getClass())
			return false;
		return Objects.equals(this.roadRef, that.roadRef) && Objects.equals(this.ref, that.ref)
				&& Objects.equals(this.name, that.name) && Objects.equals(this.distance, that.distance)
				&& Objects.equals(this.geo, that.geo);
	}
}