/*
 * Copyright © 2020 traffxml.org.
 * 
 * This file is part of the traff-libmilestone library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lib.milestone;

import org.traffxml.traff.LatLon;

/**
 * Represents a milestone, i.e.<!-- --> a distance marker.
 * 
 * <p>A distance marker has a distance and a coordinate pair.
 */
public class Milestone {
	public final Distance distance;
	public final LatLon coords;

	public Milestone(Distance distance, LatLon coords) {
		super();
		this.distance = distance;
		this.coords = coords;
	}

	public Milestone(float distance, int distanceUnit, LatLon coords) {
		this(new Distance(distance, distanceUnit), coords);
	}

	public boolean equals(Milestone that) {
		if (this == that)
			return true;
		if (that == null)
			return false;
		if (this.getClass() != that.getClass())
			return false;
		return (this.distance.equals(that.distance)) && (this.coords.equals(that.coords));
	}
}
