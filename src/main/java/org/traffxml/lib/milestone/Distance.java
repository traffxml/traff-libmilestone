/*
 * Copyright © 2020 traffxml.org.
 * 
 * This file is part of the traff-libmilestone library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lib.milestone;

/**
 * Distance indicated on a distance marker.
 * 
 * <p>A distance is specified by a numeric value and its unit.
 * 
 * <p>For two distances created with the same unit, comparison operations such as {@link #compareTo(Distance)}
 * and {@link #equals(Distance)} are guaranteed to behave in the same manner as the corresponding
 * arithmetical operators used on the corresponding raw numbers. Anomalies may occur, however, when comparing
 * two distances created with different units but effectively very similar values. Behavior in these cases is
 * not guaranteed to be stable across versions.
 */
public class Distance implements Comparable<Distance> {
	/* Unit constants are the equivalent of one unit in meters. */
	public static final int UNIT_M = 1;
	public static final int UNIT_HM = 100;
	public static final int UNIT_KM = 1000;
	public static final int UNIT_MI = 1609;

	final int rawDistance;

	public Distance(float distance, int distanceUnit) {
		super();
		this.rawDistance = MilestoneUtils.toRawDistance(distance, distanceUnit);
	}

	/**
	 * Returns the indicated distance in the given unit.
	 * 
	 * @param distanceUnit
	 */
	public float asUnit(int distanceUnit) {
		return MilestoneUtils.toDistanceWithUnit(rawDistance, distanceUnit);
	}

	public int compareTo(Distance that) {
		return this.rawDistance - that.rawDistance;
	}

	/**
	 * Returns the nominal difference between two distances.
	 * 
	 * @param that The other distance
	 * @param distanceUnit The unit for the result
	 */
	public float difference(Distance that, int distanceUnit) {
		int rawDistance = Math.abs(this.rawDistance - that.rawDistance);
		return MilestoneUtils.toDistanceWithUnit(rawDistance, distanceUnit);
	}

	public boolean equals(Distance that) {
		if (this == that)
			return true;
		if (that == null)
			return false;
		if (this.getClass() != that.getClass())
			return false;
		return (this.rawDistance == that.rawDistance);
	}
}
