/*
 * Copyright © 2020 traffxml.org.
 * 
 * This file is part of the traff-libmilestone library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lib.milestone;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentSkipListSet;

import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.LatLon;

/**
 * Helper class for working with milestones.
 */
public class MilestoneUtils {
	/**
	 * Clips a set of milestones to a bounding box.
	 * 
	 * @param in The set of milestones
	 * @param bbox The bounding box to clip to
	 * 
	 * @return A set of milestones within {@code bbox}
	 */
	public static Set<Milestone> clipTo(Set<Milestone> in, BoundingBox bbox) {
		Set<Milestone> res = new HashSet<Milestone>();
		for (Milestone inMst : in)
			if (bbox.contains(inMst.coords)) {
				res.add(inMst);
				break;
			}
		return res;
	}

	/**
	 * Clips a milestone map to a bounding box.
	 * 
	 * @param in The milestone map
	 * @param bbox The bounding box to clip to
	 * 
	 * @return A milestone map whose entries are within {@code bbox}
	 */
	public static NavigableMap<Distance, Set<Milestone>> clipTo(NavigableMap<Distance, Set<Milestone>> in, BoundingBox bbox) {
		NavigableMap<Distance, Set<Milestone>> res = new TreeMap<Distance, Set<Milestone>>();
		for (Map.Entry<Distance, Set<Milestone>> entry : in.entrySet()) {
			Set<Milestone> set = clipTo(entry.getValue(), bbox);
			if (!set.isEmpty())
				res.put(entry.getKey(), set);
		}
		return res;
	}

	/**
	 * Clusters milestones which are within a certain distance of each other.
	 * 
	 * <p>A milestone is added to a cluster if it is within {@code distance} of at least one milestone in the
	 * cluster. This also means that the distance between two randomly selected milestones from a cluster can
	 * exceed {@code distance}. Each cluster is then replaced with a single milestone, located at the center
	 * of the cluster.
	 * 
	 * <p>All milestones in the supplied set must have the same nominal distance, which is used for the cluster
	 * milestone. If this constraint is violated, an {@link IllegalArgumentException} is thrown.
	 * 
	 * <p>If the set does not contain at least 2 milestones, it is returned unaltered.
	 * 
	 * <p>BUGS:
	 * 
	 * <p>In polar regions (specifically, when one of the poles falls within the convex hull of a cluster)
	 * the center position of the cluster may be off-center.
	 * 
	 * <p>The cluster strategy mentioned before may give unexpected results for some constellations. For
	 * example, consider a ring of milestones with a radius greater than {@code distance} but each milestone
	 * within {@code distance} of its nearest two neighbors, hence they would form a cluster. Another
	 * milestone located exactly in the center of that ring would form a separate cluster. However, both
	 * clusters would subsequently be reduced to a milestone with the same coordinates.
	 * 
	 * @param in The set of milestones to cluster
	 * @param distance The distance within which two milestones are to be clustered together
	 * @param distanceUnit The unit for {@code distance}
	 * @return A set of clustered and consolidated milestones
	 */
	public static Set<Milestone> clusterWithin(Set<Milestone> in, float distance, int distanceUnit) {
		if (in.size() <= 1)
			return in;
		float kmDistance = (distanceUnit == Distance.UNIT_KM) ? distance : toDistanceWithUnit(toRawDistance(distance, distanceUnit), Distance.UNIT_KM);
		Distance mDist = null;
		ArrayList<Set<LatLon>> clusters = new ArrayList<Set<LatLon>>();
		for (Milestone milestone : in) {
			if (mDist == null)
				mDist = milestone.distance;
			else if (!mDist.equals(milestone.distance))
				throw new IllegalArgumentException(
						String.format("Milestones in Set must refer to the same distance (got %.3f and %.3f)",
								mDist.asUnit(Distance.UNIT_KM), milestone.distance.asUnit(Distance.UNIT_KM)));
			SortedSet<Integer> inClusters = new ConcurrentSkipListSet<Integer>();
			for (int i = 0; i < clusters.size(); i++)
				for (LatLon llic : clusters.get(i))
					if (milestone.coords.distanceTo(llic) <= kmDistance) {
						clusters.get(i).add(milestone.coords);
						inClusters.add(i);
						break; // for llic
					}
			if (inClusters.isEmpty()) {
				/* no matching clusters for the milestone, create a new one */
				Set<LatLon> set = new HashSet<LatLon>();
				set.add(milestone.coords);
				clusters.add(set);
			} else
				/* if the milestone got added to multiple clusters, merge them */
				while (inClusters.size() > 1) {
					Integer last = inClusters.last();
					clusters.get(inClusters.first()).addAll(clusters.get(last));
					clusters.remove(last.intValue());
					inClusters.remove(last);
				}
		}
		/* consolidate each cluster into one milestone */
		HashSet<Milestone> res = new HashSet<Milestone>();
		for (Set<LatLon> set : clusters)
			if (set.size() <= 1)
				for (LatLon ll : set)
					res.add(new Milestone(mDist, ll));
			else {
				BoundingBox bbox = new BoundingBox(set);
				float lat = (bbox.minLat + bbox.maxLat) / 2.0f;
				float lon = (bbox.minLon + bbox.maxLon) / 2.0f;
				if (bbox.minLon > bbox.maxLon)
					lon -= 180.0f * Math.signum(lon);
				res.add(new Milestone(mDist, new LatLon(lat, lon)));
			}
		return res;
	}

	/**
	 * Clusters milestones which are within a certain distance of each other.
	 * 
	 * <p>This is a convenience wrapper around {@link #clusterWithin(Set, float, int)}, which it calls once for
	 * each map entry, passing the value of the entry and adding it to the result with the key of the
	 * original entry.
	 * 
	 * @param in The map of milestones to cluster
	 * @param distance The distance within which two milestones are to be clustered together
	 * @param distanceUnit The unit for {@code distance}
	 * @return A map of clustered and consolidated milestones
	 */
	public static NavigableMap<Distance, Set<Milestone>> clusterWithin(Map<Distance, Set<Milestone>> in, float distance, int distanceUnit) {
		NavigableMap<Distance, Set<Milestone>> res = new TreeMap<Distance, Set<Milestone>>();
		for (Map.Entry<Distance, Set<Milestone>> entry : in.entrySet())
			res.put(entry.getKey(), clusterWithin(entry.getValue(), distance, distanceUnit));
		return res;
	}

	/**
	 * Eliminates all milestones whose distance from a reference location exceeds the specified threshold.
	 * 
	 * @param in The set of milestones
	 * @param ref The reference location
	 * @param distance The maximum distance, in the unit specified by {@code distanceUnit}
	 * @param distanceUnit
	 * 
	 * @return A set of the locations in {@code in} which are no further than {@code distance} away from {@code ref}.
	 */
	public static Set<Milestone> eliminate(Set<Milestone> in, LatLon ref, float distance, int distanceUnit) {
		Set<Milestone> res = new HashSet<Milestone>();
		float distanceKm = toDistanceWithUnit(toRawDistance(distance, distanceUnit), Distance.UNIT_KM);
		for (Milestone milestone : in)
			if (ref.distanceTo(milestone.coords) <= distanceKm)
				res.add(milestone);
		// FIXME debug
		/*
			else
				System.err.printf("Milestone %.1f (%.5f, %.5f) is %.5f away from ref, limit is %.3f, discarding\n",
						milestone.distance.asUnit(Distance.UNIT_KM),
						milestone.coords.lat,
						milestone.coords.lon,
						(float) ref.distanceTo(milestone.coords),
						distanceKm);
						*/ 
		// FIXME end debug
		return res;
	}

	/**
	 * Eliminates all milestones whose distance from a reference location exceeds the specified threshold.
	 * 
	 * <p>This is a convenience wrapper around {@link #eliminate(Set, LatLon, float, int)}, which it calls once for
	 * each map entry, passing the value of the entry and adding it to the result with the key of the
	 * original entry. If elimination creates empty sets, the entry is dropped.
	 * 
	 * @param in The set of milestones
	 * @param ref The reference location
	 * @param distance The maximum distance, in the unit specified by {@code distanceUnit}
	 * @param distanceUnit
	 * 
	 * @return A map of the locations in {@code in} which are no further than {@code distance} away from {@code ref}.
	 */
	public static NavigableMap<Distance, Set<Milestone>> eliminate(Map<Distance, Set<Milestone>> in, LatLon ref, float distance, int distanceUnit) {
		NavigableMap<Distance, Set<Milestone>> res = new TreeMap<Distance, Set<Milestone>>();
		for (Map.Entry<Distance, Set<Milestone>> entry : in.entrySet()) {
			Set<Milestone> set = eliminate(entry.getValue(), ref, distance, distanceUnit);
			if (!set.isEmpty())
				res.put(entry.getKey(), set);
		}
		return res;
	}

	/**
	 * Eliminates all milestones who are inconsistent with the reference set.
	 * 
	 * <p>A milestone is considered to be inconsistent with the reference set if the reference set does not
	 * contain a milestone which is no further from the milestone being examined than indicated by the
	 * difference between their nominal distance plus the specified tolerance value.
	 * 
	 * @param in The set to eliminate from
	 * @param ref The reference set for comparison
	 * @param tolerance The tolerance, in the unit specified by {@code toleranceUnit}
	 * @param toleranceUnit
	 */
	public static Set<Milestone> eliminateInconsistent(Set<Milestone> in, Set<Milestone> ref, float tolerance, int toleranceUnit) {
		Set<Milestone> res = new HashSet<Milestone>();
		float toleranceKm = toDistanceWithUnit(toRawDistance(tolerance, toleranceUnit), Distance.UNIT_KM);
		for (Milestone inMst : in)
			for (Milestone refMst : ref)
				if (inMst.coords.distanceTo(refMst.coords) <= (inMst.distance.difference(refMst.distance, Distance.UNIT_KM) + toleranceKm)) {
					res.add(inMst);
					break;
				}
		return res;
	}

	/**
	 * Eliminates all milestones who are inconsistent with the reference set.
	 * 
	 * <p>This is a convenience wrapper around {@link #eliminateInconsistent(Set, Set, float, int)}, which it calls once for
	 * each map entry, passing the value of the entry and adding it to the result with the key of the
	 * original entry. If elimination creates empty sets, the entry is dropped.
	 * 
	 * @param in The map to eliminate from
	 * @param ref The reference set for comparison
	 * @param tolerance The tolerance, in the unit specified by {@code toleranceUnit}
	 * @param toleranceUnit
	 */
	public static NavigableMap<Distance, Set<Milestone>> eliminateInconsistent(Map<Distance, Set<Milestone>> in, Set<Milestone> ref, float tolerance, int toleranceUnit) {
		NavigableMap<Distance, Set<Milestone>> res = new TreeMap<Distance, Set<Milestone>>();
		for (Map.Entry<Distance, Set<Milestone>> entry : in.entrySet()) {
			Set<Milestone> set = eliminateInconsistent(entry.getValue(), ref, tolerance, toleranceUnit);
			if (!set.isEmpty())
				res.put(entry.getKey(), set);
		}
		return res;
	}

	/**
	 * Eliminates all milestones who are redundant with respect to the reference set.
	 * 
	 * <p>A milestone is considered to be redundant with respect to the reference set if the reference set
	 * contains a milestone which is no further from the milestone being examined than indicated by the
	 * difference between their nominal distance plus the specified tolerance value.
	 * 
	 * <p>It is recommended to cluster both {@code in} and {@code ref} prior to calling this method.
	 * 
	 * @param in The set to eliminate from
	 * @param ref The reference set for comparison
	 * @param tolerance The tolerance, in the unit specified by {@code toleranceUnit}
	 * @param toleranceUnit
	 */
	public static Set<Milestone> eliminateRedundant(Set<Milestone> in, Set<Milestone> ref, float tolerance, int toleranceUnit) {
		Set<Milestone> res = new HashSet<Milestone>();
		float toleranceKm = toDistanceWithUnit(toRawDistance(tolerance, toleranceUnit), Distance.UNIT_KM);
		for (Milestone inMst : in) {
			boolean redundant = false;
			for (Milestone refMst : ref)
				if (inMst.coords.distanceTo(refMst.coords) <= (inMst.distance.difference(refMst.distance, Distance.UNIT_KM) + toleranceKm)) {
					redundant = true;
					break;
				}
			if (!redundant)
				res.add(inMst);
		}
		return res;
	}

	/**
	 * Eliminates pairs of redundant milestones between the two sides of {@code distance}, keeping only the closer one.
	 * 
	 * <p>For the definition of redundancy, see {@link #eliminateRedundant(Set, Set, float, int)}.
	 * 
	 * <p>This method assumes {@link #eliminateRedundantPerSide(NavigableMap, Distance, float, int)} has already
	 * been run against {@code in}, so that groups of redundant milestones are always pairs of two milestones
	 * located on different sides of {@code distance}.
	 * 
	 * <p>Interpolation should be performed before rather than after this function, as interpolation typically
	 * relies upon redundant pairs of points (and eliminates them in the process).
	 * 
	 * <p>FIXME: how do we resolve ties (e.g. distance 22.5 with redundant milestones at 22 and 23)?
	 * 
	 * <p>This method operates on a map of milestones, starting at {@code distance} and moving outwards in each
	 * direction. Milestones are discarded if they are found to be redundant with respect to the reference
	 * set, which considers of all milestones between the current distance and {@code distance}. A milestone
	 * with a distance greater than {@code distance} and another with a distance less than {@code distance}
	 * will never be considered redundant.
	 * 
	 * <p>Its intended use case is a set of milestones presenting the usual common anomalies: different points
	 * on the road may have the same nominal distance, and not all milestones may be present in the set.
	 * 
	 * <p>It is recommended to cluster {@code in} prior to calling this method.
	 * 
	 * @param map The map to eliminate from (this map will be altered)
	 * @param distance
	 * @param tolerance
	 * @param toleranceUnit
	 * @param preferLower Whether to keep milestone with the lower distance if both milestones in a redundant
	 * pair are equally far from {@code distance}; if false, the milestone with the higher distance is kept 
	 */
	public static void eliminateRedundantBetweenSides(NavigableMap<Distance, Set<Milestone>> map,
			Distance distance, float tolerance, int toleranceUnit, boolean preferLower) {
		for (Distance currLower = map.lowerKey(distance); currLower != null; currLower = map.lowerKey(currLower)) {
			float lowerDiff = currLower.difference(distance, Distance.UNIT_M);
			for (Distance currHigher = map.higherKey(distance); currHigher != null; currHigher = map.higherKey(currHigher)) {
				float higherDiff = currHigher.difference(distance, Distance.UNIT_M);
				if ((lowerDiff < higherDiff) || (preferLower && (lowerDiff == higherDiff)))
					map.put(currHigher, eliminateRedundant(map.get(currHigher), map.get(currLower), tolerance, toleranceUnit));
				else if ((lowerDiff > higherDiff) || (!preferLower && (lowerDiff == higherDiff)))
					map.put(currLower, eliminateRedundant(map.get(currLower), map.get(currHigher), tolerance, toleranceUnit));
			}
		}
		stripEmpty(map);
	}

	/**
	 * Eliminates all redundant milestones on each side of {@code distance}, keeping only the closest one on each side.
	 * 
	 * <p>For the definition of redundancy, see {@link #eliminateRedundant(Set, Set, float, int)}.
	 * 
	 * <p>This method operates on a map of milestones, starting at {@code distance} and moving outwards in each
	 * direction. Milestones are discarded if they are found to be redundant with respect to the reference
	 * set, which considers of all milestones between the current distance and {@code distance}. A milestone
	 * with a distance greater than {@code distance} and another with a distance less than {@code distance}
	 * will never be considered redundant.
	 * 
	 * <p>Its intended use case is a set of milestones presenting the usual common anomalies: different points
	 * on the road may have the same nominal distance, and not all milestones may be present in the set.
	 * 
	 * <p>It is recommended to cluster {@code in} prior to calling this method.
	 * 
	 * @param in The map to eliminate from
	 * @param distance
	 * @param tolerance
	 * @param toleranceUnit
	 */
	public static NavigableMap<Distance, Set<Milestone>> eliminateRedundantPerSide(NavigableMap<Distance, Set<Milestone>> in,
			Distance distance, float tolerance, int toleranceUnit) {
		NavigableMap<Distance, Set<Milestone>> res = new TreeMap<Distance, Set<Milestone>>();
		Set<Milestone> ref = new HashSet<Milestone>();
		for (Distance curr = in.floorKey(distance); curr != null; curr = in.lowerKey(curr)) {
			Set<Milestone> set = ref.isEmpty() ? new HashSet<Milestone>(in.get(curr)) : eliminateRedundant(in.get(curr), ref, tolerance, toleranceUnit);
			if (!set.isEmpty()) {
				res.put(curr, set);
				ref.addAll(set);
			}
		}
		/* same procedure, other half and other direction */
		ref = new HashSet<Milestone>();
		for (Distance curr = in.ceilingKey(distance); curr != null; curr = in.higherKey(curr)) {
			Set<Milestone> set = ref.isEmpty() ? new HashSet<Milestone>(in.get(curr)) : eliminateRedundant(in.get(curr), ref, tolerance, toleranceUnit);
			if (!set.isEmpty()) {
				res.put(curr, set);
				ref.addAll(set);
			}
		}
		return res;
	}

	/**
	 * Tries to interpolate a given distance from surrounding milestones.
	 * 
	 * <p>For each successful interpolation, a new milestone with a distance matching {@code distance} is added
	 * to {@code map}, and the milestones used for interpolation are removed.
	 * 
	 * <p>If a non-negative tolerance is set, interpolation will only be attempted if the accuracy of the result
	 * (expressed as a distance between actual and calculated location) is within the tolerance limit.
	 * 
	 * <p>Prior to calling this method, the caller should cluster the milestones in the set and eliminate
	 * inconsistent ones.
	 * 
	 * <p>BUGS:
	 * 
	 * <p>Tolerance is implemented in a somewhat crude (and likely mathematically incorrect) manner.
	 * 
	 * <p>Interpolation suffers from the usual weaknesses of projecting the world into a rectangle: Interpolated
	 * points may deviate from the great circle, though this should be negligible when interpolating over
	 * distances below 100 km (which is the intended use case here). It will produce incorrect results near
	 * the poles and when crossing the +/-180 degree meridian. With tolerance, this will likely result in a
	 * perfectly good result being discarded.
	 * 
	 * <p>Any discontinuity in the numbering of milestones, such as missing or duplicate ones, can greatly
	 * confuse this method and lead to bogus results. Tolerance alleviates some of these cases, while some
	 * others can be caught through consistency checks against a reference location, but some cases may still
	 * slip through.
	 * 
	 * @param map A map of milestones, e.g. as returned by {@code MilestoneList#getEnclosingOrPoint(String, Distance)}
	 * @param distance
	 * @param tolerance Maximum distance between the returned location and the actual location on the road,
	 * in the unit specified by {@code toleranceUnit}; negative values indicate no tolerance
	 * @param toleranceUnit
	 */
	public static void interpolate(NavigableMap<Distance, Set<Milestone>> map, Distance distance,
			float tolerance, int toleranceUnit) {
		/*
		 * FIXME
		 * This may produce bogus results in examples like the following:
		 * Sections are numbered 0–585 (A), 0–16 (B), 0–16 (again, C), 615–800 (D).
		 * The road is perfectly straight throughout sections C and D, as well as km 0–8A.
		 * We have km 0A, 8A, 0B, 8B, 0C and 8C in the set.
		 * We are looking for km 4 (but no information on the section).
		 * 
		 * 0A/8A are consistent and will be interpolated to 4A.
		 * In the same manner, 0B/8B and 0C/8C will be interpolated to 4B and 4C, respectively.
		 * 0A and 8A are not consistent with anything else (which would be several hundred km away), and
		 * 0B/8C are also not consistent as they are 24 km from each other.
		 * 
		 * However, 0C/8B would be consistent and interpolated to a location close to 12B.
		 * If we assume another example in which duplicate milestones with different references happen to be
		 * consistent due to the curvature of the road, the interpolated result would even be off-road.
		 * 
		 * In practice this might be a corner case, as two milestones from different sections are highly
		 * unlikely to be consistent if a realistic tolerance (~100 m) is used.
		 * 
		 * If reference coordinates are available, these may filter out some of the bogus results.
		 */
		stripEmpty(map);
		Set<Milestone> atDist = map.get(distance);
		Set<Milestone> obsoleted = new HashSet<Milestone>();
		if (atDist == null ) {
			atDist = new HashSet<Milestone>();
			map.put(distance, atDist);
		}
		for (Distance lowerKey = map.lowerKey(distance); lowerKey != null; lowerKey = map.lowerKey(lowerKey))
			for (Milestone lower : map.get(lowerKey))
				for (Distance higherKey = map.higherKey(distance); higherKey != null; higherKey = map.higherKey(higherKey)) {
					float nominalDist = lowerKey.difference(higherKey, toleranceUnit);
					float lowerWeight = distance.difference(higherKey, toleranceUnit) / nominalDist;
					for (Milestone higher : map.get(higherKey)) {
						/* Actual distance between enclosing milestones */
						float actualDist = toDistanceWithUnit(toRawDistance((float) lower.coords.distanceTo(higher.coords),
								Distance.UNIT_KM), toleranceUnit);
						float diff = Math.abs(nominalDist - actualDist);
						/* Whether the point is close to one of the milestones being examined */
						boolean isCloseToMilestone = false;
						if ((diff <= tolerance) || (actualDist < nominalDist)) {
							/* Estimated distance to nearest end point (nominal multiplied with error factor) */
							float endpointDist = Math.min(distance.difference(lower.distance, toleranceUnit),
									distance.difference(higher.distance, toleranceUnit))
									* Math.max(nominalDist, actualDist)
									/ Math.min(nominalDist, actualDist);
							if (endpointDist <= tolerance)
								isCloseToMilestone = true;
						}
						if ((tolerance >= 0) && (diff > tolerance) && !isCloseToMilestone)
							continue;
						atDist.add(new Milestone(distance, new LatLon(lower.coords.lat * lowerWeight + higher.coords.lat * (1 - lowerWeight),
								lower.coords.lon * lowerWeight + higher.coords.lon * (1 - lowerWeight))));
						obsoleted.add(lower);
						obsoleted.add(higher);
					}
				}
		for (Set<Milestone> entries : map.values())
			entries.removeAll(obsoleted);
		stripEmpty(map);
	}

	/**
	 * Merges milestones from a set into a map if they are consistent with the target map.
	 * 
	 * <p>Milestones from {@code set} are added to {@code map} if it does not already contain milestones for
	 * the same distance, and the milestone to be inserted is consistent with its nearest neighbors in
	 * {@code map}.
	 * 
	 * <p>This is intended for use cases in which two data sets of different accuracy are to be merged. The
	 * approach is to merge the less accurate (but potentially more complete) set into the more accurate one,
	 * resolving duplicates in favor of the more accurate set and rejecting entries which are inconsistent
	 * with it. It can be used to fill gaps in an accurate but incomplete data set with one from a less
	 * accurate but potentially more complete one.
	 * 
	 * <p>One such example would be an incomplete set of milestones and a fairly complete set of distances
	 * inferred from junctions with distance-based numbering (which can be as much as 500 m off): first
	 * obtain the milestone data, then merge the junction-based data into the set.
	 * 
	 * <p>Another example would be if one set has milestones per section of a road (assuming that each
	 * kilometric point equation starts a new section) but lacks some milestones; another has milestones per
	 * road, with better coverage but no information on the section to which a particular milestone belongs.
	 * First obtain the milestones for the section, then merge the milestones for the entire road.
	 * 
	 * @param map The target map (will be written to)
	 * @param set The set of milestones to add
	 * @param tolerance The tolerance, in the unit specified by {@code toleranceUnit}
	 * @param toleranceUnit
	 */
	public static void mergeConsistent(NavigableMap<Distance, Set<Milestone>> map,
			Set<Milestone> set, float tolerance, int toleranceUnit) {
		float toleranceKm = toDistanceWithUnit(toRawDistance(tolerance, toleranceUnit), Distance.UNIT_KM);
		Set<Milestone> toAdd = new HashSet<Milestone>();
		for (Milestone setMst : set) {
			if (map.containsKey(setMst.distance))
				continue;
			NavigableMap<Distance, Set<Milestone>> subMap = map.subMap(
					(map.floorKey(setMst.distance) != null ? map.floorKey(setMst.distance) : setMst.distance), true,
					(map.ceilingKey(setMst.distance) != null ? map.ceilingKey(setMst.distance) : setMst.distance), true);
			/* Default is consistent if we have nothing to compare to */
			boolean isConsistent = true;
			for (Entry<Distance, Set<Milestone>> refEntry : subMap.entrySet()) {
				isConsistent = false;
				for (Milestone refMst : refEntry.getValue())
					if (setMst.coords.distanceTo(refMst.coords) <= (setMst.distance.difference(refMst.distance, Distance.UNIT_KM) + toleranceKm)) {
						isConsistent = true;
						break;
					}
				if (!isConsistent)
					break;
			}
			if (isConsistent)
				toAdd.add(setMst);
		}
		/* add milestones found to be consistent */
		for (Milestone setMst : toAdd) {
			Set<Milestone> entrySet = map.get(setMst.distance);
			if (entrySet == null) {
				entrySet = new HashSet<Milestone>();
				map.put(setMst.distance, entrySet);
			}
			entrySet.add(setMst);
		}
	}

	/**
	 * Strips empty or null values from a map of milestones.
	 * 
	 * <p>If the value associated with a key (a distance) in the map is either null or an empty set, its key is
	 * removed from the map.
	 * 
	 * @param map The map from which to strip empty values
	 */
	public static void stripEmpty(Map<Distance, Set<Milestone>> map) {
		Set<Distance> emptyDists = new HashSet<Distance>();
		for (Map.Entry<Distance, Set<Milestone>> entry : map.entrySet())
			if ((entry.getValue() == null) || entry.getValue().isEmpty())
				emptyDists.add(entry.getKey());
		for (Distance distance : emptyDists)
			map.remove(distance);
	}

	/**
	 * Converts internal distance to a value in the given unit.
	 * 
	 * @param rawDistance Distance as stored internally
	 * @param distanceUnit The unit for the distance
	 * @return Distance in the uit specified by {@code distanceUnit}
	 */
	static float toDistanceWithUnit(int rawDistance, int distanceUnit) {
		return ((float) rawDistance) / ((float) distanceUnit);
	}

	/**
	 * Converts distance in the given unit to its internal value.
	 * 
	 * @param distance Distance in the unit specified by {@code distanceUnit}
	 * @param distanceUnit The unit for the distance
	 * @return A distance value that can be used for internal storage and calculations
	 */
	static int toRawDistance(float distance, int distanceUnit) {
		return Math.round(distance * distanceUnit);
	}

	/**
	 * Consolidates a map of milestone sets into a single set.
	 * 
	 * @param in A map
	 * @return A set which contains all milestones contained in at least one value of {@code in}
	 */
	public static Set<Milestone> toSet(Map<Distance, Set<Milestone>> in) {
		Set<Milestone> res = new HashSet<Milestone>();
		for (Set<Milestone> set : in.values())
			res.addAll(set);
		return res;
	}
}
