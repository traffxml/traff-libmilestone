[![Release](https://jitpack.io/v/org.traffxml/traff-libmilestone.svg)](https://jitpack.io/#org.traffxml/traff-libmilestone)

Javadoc for `master` is at https://traffxml.gitlab.io/traff-libmilestone/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-libmilestone/javadoc/dev/.
