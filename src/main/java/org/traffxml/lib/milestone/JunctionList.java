/*
 * Copyright © 2020 traffxml.org.
 * 
 * This file is part of the traff-libmilestone library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lib.milestone;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;
import org.traffxml.traff.LatLon;

/**
 * A list of junctions in a road network.
 * 
 * <p>A road network typically comprises all roads in a given geographical area, optionally limited to
 * certain tiers. Technically the only constraint is that each road must have a unique reference number
 * within the network.
 */
public class JunctionList {
	/**
	 * Map of junctions by road reference and junction name.
	 * 
	 * <p>The key is the road reference. The data is another map, with the junction name as the key and a set
	 * of all junctions (typically one per direction) on that road.
	 */
	Map<String, Map<String, Set<Junction>>> junctionsByName =
			new HashMap<String, Map<String, Set<Junction>>>();

	/**
	 * Map of junctions by road reference and junction reference.
	 * 
	 * <p>The key is the road reference. The data is another map, with the junction name as the key and a set
	 * of all junctions (typically one per direction) on that road.
	 */
	Map<String, Map<String, Set<Junction>>> junctionsByRef =
			new HashMap<String, Map<String, Set<Junction>>>();

	/**
	 * Reads a new list of junctions from a stream.
	 * 
	 * <p>The {@code nffDelimiter} is an additional delimiter for CSV files which do not follow the first normal
	 * form, i.e. which allow multiple values inside a single CSV field. Currently this is only allowed for
	 * the road ref: if {@code nffDelimiter = ';'} and a record with road ref {@code A1;S8} is encountered,
	 * it will generate two entries, one for {@code A1} and one for {@code S8}.
	 * 
	 * @param delimiter The delimiter character used in the input file
	 * @param nffDelimiter Additional delimiter for multiple values inside a CSV field, see description
	 * @param stream The stream to read from
	 * @param latName The column name for latitude information
	 * @param lonName The column name for longitude information
	 * @param roadRefName The column name for the road reference number
	 * @param junctionRefName The column name for the junction reference number, or null if there is no such column
	 * @param nameName The column name for the junction name
	 * @param distName The column name for the distance values, or null if there is no such column
	 * @param distUnit One of the {@code UNIT_*} constants in {@link Distance} denoting the unit used for distances
	 */
	// FIXME add coordinate system (current assumption is WGS84)
	public JunctionList(InputStream stream, char delimiter, Character nffDelimiter, String latName, String lonName, String roadRefName,
			String junctionRefName, String nameName, String distName, int distUnit) throws IOException {
		super();
		InputStreamReader reader = new InputStreamReader(new BOMInputStream(stream));
		Iterable<CSVRecord> records;
		records = CSVFormat.DEFAULT.withDelimiter(delimiter).withFirstRecordAsHeader().parse(reader);
		for (CSVRecord record : records)
			try {
				String name = strip(record.get(nameName));
				if (name == null)
					continue;
				float latitude = Float.valueOf(record.get(latName));
				float longitude = Float.valueOf(record.get(lonName));
				String roadRefString = strip(record.get(roadRefName));
				String junctionRef = (junctionRefName == null) ? null : strip(record.get(junctionRefName));
				String distanceString = (distName == null) ? null : strip(record.get(distName));
				Distance distance = (distanceString == null) ? null : new Distance(Float.valueOf(distanceString.replace(',', '.')), distUnit);
				String[] roadRefs = {roadRefString};
				if (nffDelimiter != null)
					roadRefs = roadRefString.split(nffDelimiter.toString());
				for (String roadRef : roadRefs) {
					Map<String, Set<Junction>> roadJunctions;
					Junction junction = new Junction(roadRef, junctionRef, name, distance, new LatLon(latitude, longitude));

					// by name
					roadJunctions = junctionsByName.get(roadRef.trim());
					if (roadJunctions == null)  {
						roadJunctions = new HashMap<String, Set<Junction>>();
						junctionsByName.put(roadRef.trim(), roadJunctions);
					}
					Set<Junction> namedJunctions = roadJunctions.get(name);
					if (namedJunctions == null) {
						namedJunctions = new HashSet<Junction>();
						roadJunctions.put(name, namedJunctions);
					}
					namedJunctions.add(junction);

					// by ref
					roadJunctions = junctionsByRef.get(roadRef.trim());
					if (roadJunctions == null)  {
						roadJunctions = new HashMap<String, Set<Junction>>();
						junctionsByRef.put(roadRef.trim(), roadJunctions);
					}
					Set<Junction> refedJunctions = roadJunctions.get(junctionRef);
					if (refedJunctions == null) {
						refedJunctions = new HashSet<Junction>();
						roadJunctions.put(junctionRef, refedJunctions);
					}
					refedJunctions.add(junction);
				}
			} catch (Exception e) {
				continue;
			}
	}

	/**
	 * Returns the meaningful parts of a string.
	 * 
	 * <p>If {@code string} is null, empty or only whitespace, null is returned. Otherwise, leading and trailing
	 * whitespace characters are stripped from {@code string} and the result returned.
	 * 
	 * @param string The string to convert
	 * @return See description
	 */
	private static String strip(String string) {
		if (string == null)
			return null;
		String res = string.trim();
		if (res.isEmpty())
			return null;
		else
			return res;
	}

	/**
	 * Returns all junctions for a given road.
	 * 
	 * @param roadRef
	 * @return A set of junctions on the requested road
	 */
	public Map<String, Set<Junction>> getByName(String roadRef) {
		Map<String, Set<Junction>> roadJunctions = junctionsByName.get(roadRef);
		if (roadJunctions == null)
			return null;
		Map<String, Set<Junction>> res = new HashMap<String, Set<Junction>>();
		/*
		 * Do not expose the original map and sets, as any changes callers might make to them would alter our
		 * master data. Distance and Junction instances are OK to pass, as they are immutable.
		 */
		for (Map.Entry<String, Set<Junction>> entry : roadJunctions.entrySet())
			res.put(entry.getKey(), new HashSet<Junction>(entry.getValue()));
		return res;
	}

	/**
	 * Returns all junctions for a given road.
	 * 
	 * @param roadRef
	 * @return A set of junctions on the requested road
	 */
	public Map<String, Set<Junction>> getByRef(String roadRef) {
		Map<String, Set<Junction>> roadJunctions = junctionsByRef.get(roadRef);
		if (roadJunctions == null)
			return null;
		Map<String, Set<Junction>> res = new HashMap<String, Set<Junction>>();
		/*
		 * Do not expose the original map and sets, as any changes callers might make to them would alter our
		 * master data. Distance and Junction instances are OK to pass, as they are immutable.
		 */
		for (Map.Entry<String, Set<Junction>> entry : roadJunctions.entrySet())
			res.put(entry.getKey(), new HashSet<Junction>(entry.getValue()));
		return res;
	}

	/**
	 * Gets the coordinates of a junction.
	 * 
	 * @param roadRef The road reference
	 * @param name The junction name
	 * @return The average of all coordinate pairs, or null if no entries were found
	 */
	public LatLon getCoordsByName(String roadRef, String name) {
		float latSum = 0;
		float lonSum = 0;
		float count = 0;
		Map<String, Set<Junction>> roadJunctions = junctionsByName.get(roadRef);
		if (roadJunctions == null)
			return null;
		Set<Junction> namedJunctions = roadJunctions.get(name);
		if (namedJunctions == null)
			return null;
		for (Junction junction : namedJunctions) {
			latSum += junction.geo.lat;
			lonSum += junction.geo.lon;
			count++;
		}
		if (count == 0)
			return null;
		try {
			return new LatLon(latSum / count, lonSum / count);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	/**
	 * Gets the coordinates of a junction.
	 * 
	 * @param roadRef The road reference
	 * @param junctionRef The junction reference
	 * @return The average of all coordinate pairs, or null if no entries were found
	 */
	public LatLon getCoordsByRef(String roadRef, String junctionRef) {
		float latSum = 0;
		float lonSum = 0;
		float count = 0;
		Map<String, Set<Junction>> roadJunctions = junctionsByRef.get(roadRef);
		if (roadJunctions == null)
			return null;
		Set<Junction> namedJunctions = roadJunctions.get(junctionRef);
		if (namedJunctions == null)
			return null;
		for (Junction junction : namedJunctions) {
			latSum += junction.geo.lat;
			lonSum += junction.geo.lon;
			count++;
		}
		if (count == 0)
			return null;
		try {
			return new LatLon(latSum / count, lonSum / count);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	/**
	 * Gets the kilometric point of a junction.
	 * 
	 * <p>If multiple entries are present with different distances, the average of all values is returned.
	 * If no kilometric points are associated with that road/junction pair, null is returned.
	 * 
	 * @param roadRef The road reference
	 * @param name The junction name
	 * @return The average of all kilometric points, or null if no entries were found
	 */
	public Distance getDistanceByName(String roadRef, String name) {
		float sum = 0;
		float count = 0;
		Map<String, Set<Junction>> roadJunctions = junctionsByName.get(roadRef);
		if (roadJunctions == null)
			return null;
		Set<Junction> namedJunctions = roadJunctions.get(name);
		if (namedJunctions == null)
			return null;
		for (Junction junction : namedJunctions)
			if (junction.distance != null) {
				sum += junction.distance.asUnit(Distance.UNIT_M);
				count++;
			}
		if (count == 0)
			return null;
		return new Distance(sum / count, Distance.UNIT_M);
	}

	/**
	 * Gets the kilometric point of a junction.
	 * 
	 * <p>If multiple entries are present with different distances, the average of all values is returned.
	 * If no kilometric points are associated with that road/junction pair, null is returned.
	 * 
	 * @param roadRef The road reference
	 * @param junctionRef The junction reference
	 * @return The average of all kilometric points, or null if no entries were found
	 */
	public Distance getDistanceByRef(String roadRef, String junctionRef) {
		float sum = 0;
		float count = 0;
		Map<String, Set<Junction>> roadJunctions = junctionsByRef.get(roadRef);
		if (roadJunctions == null)
			return null;
		Set<Junction> namedJunctions = roadJunctions.get(junctionRef);
		if (namedJunctions == null)
			return null;
		for (Junction junction : namedJunctions)
			if (junction.distance != null) {
				sum += junction.distance.asUnit(Distance.UNIT_M);
				count++;
			}
		if (count == 0)
			return null;
		return new Distance(sum / count, Distance.UNIT_M);
	}
}
