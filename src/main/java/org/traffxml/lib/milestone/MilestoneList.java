/*
 * Copyright © 2020 traffxml.org.
 * 
 * This file is part of the traff-libmilestone library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lib.milestone;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;
import org.traffxml.traff.LatLon;

/**
 * A list of milestones in a road network.
 * 
 * <p>A road network typically comprises all roads in a given geographical area, optionally limited to
 * certain tiers. Technically the only constraint is that each road must have a unique reference number
 * within the network.
 */
public class MilestoneList {
	/**
	 * Map of milestonesm
	 * 
	 * <p>The key is the road reference. The data is another map, with the kilometric point as the key and a set
	 * of {@link LatLon} pairs as the data. This nesting is required because the combination of road ref and
	 * kilometric point is not always unambiguous.
	 */
	private Map<String, NavigableMap<Distance, Set<Milestone>>> milestones =
			new HashMap<String, NavigableMap<Distance, Set<Milestone>>>();

	/**
	 * Reads a new list of milestones from a stream.
	 * 
	 * <p>The {@code nffDelimiter} is an additional delimiter for CSV files which do not follow the first normal
	 * form, i.e. which allow multiple values inside a single CSV field. Currently this is only allowed for
	 * the road ref: if {@code nffDelimiter = ';'} and a record with road ref {@code A1;S8} is encountered,
	 * it will generate two entries, one for {@code A1} and one for {@code S8}.
	 * 
	 * @param delimiter The delimiter character used in the input file
	 * @param nffDelimiter Additional delimiter for multiple values inside a CSV field, see description
	 * @param stream The stream to read from
	 * @param latName The column name for latitude information
	 * @param lonName The column name for longitude information
	 * @param roadRefName The column name for the road reference number
	 * @param distName The column name for the distance values
	 * @param distUnit One of the {@code UNIT_*} constants in {@link Distance} denoting the unit used for distances
	 */
	// FIXME add coordinate system (current assumption is WGS84)
	public MilestoneList(InputStream stream, char delimiter, Character nffDelimiter, String latName, String lonName, String roadRefName,
			String distName, int distUnit) throws IOException {
		super();
		InputStreamReader reader = new InputStreamReader(new BOMInputStream(stream));
		Iterable<CSVRecord> records;
		records = CSVFormat.DEFAULT.withDelimiter(delimiter).withFirstRecordAsHeader().parse(reader);
		for (CSVRecord record : records)
			try {
				if (record.get(distName).trim().isEmpty())
					continue;
				float latitude = Float.valueOf(record.get(latName));
				float longitude = Float.valueOf(record.get(lonName));
				String roadRefString = record.get(roadRefName).trim();
				Distance distance = new Distance(Float.valueOf(record.get(distName).replace(',', '.')), distUnit);
				String[] roadRefs = {roadRefString};
				if (nffDelimiter != null)
					roadRefs = roadRefString.split(nffDelimiter.toString());
				for (String roadRef : roadRefs) {
					NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
					if (roadMilestones == null) {
						roadMilestones = new TreeMap<Distance, Set<Milestone>>();
						milestones.put(roadRef, roadMilestones);
					}
					Set<Milestone> milestoneCoords = roadMilestones.get(distance);
					if (milestoneCoords == null) {
						milestoneCoords = new HashSet<Milestone>();
						roadMilestones.put(distance, milestoneCoords);
					}
					milestoneCoords.add(new Milestone(distance, new LatLon(latitude, longitude)));
				}
			} catch (Exception e) {
				continue;
			}
	}

	/**
	 * Returns the milestones having exactly the indicated distance.
	 * 
	 * @param roadRef
	 * @param distance
	 * @return A set of milestones on the requested road at the requested distance
	 */
	public Set<Milestone> get(String roadRef, Distance distance) {
		Set<Milestone> res = new HashSet<Milestone>();
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
		if (roadMilestones != null)
			res.addAll(roadMilestones.get(distance));
		return res;
	}

	/**
	 * Returns all milestones for a given road.
	 * 
	 * @param roadRef
	 * @return A set of milestones on the requested road
	 */
	public NavigableMap<Distance, Set<Milestone>> get(String roadRef) {
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
		if (roadMilestones == null)
			return null;
		NavigableMap<Distance, Set<Milestone>> res = new TreeMap<Distance, Set<Milestone>>();
		/*
		 * Do not expose the original map and sets, as any changes callers might make to them would alter our
		 * master data. Distance and Milestone instances are OK to pass, as they are immutable.
		 */
		for (Map.Entry<Distance, Set<Milestone>> entry : roadMilestones.entrySet())
			res.put(entry.getKey(), new HashSet<Milestone>(entry.getValue()));
		return res;
	}

	/**
	 * Returns all milestones for a given set of road refs.
	 * 
	 * <p>This method is intended for use cases where a road is known by multiple identifiers. It will treat all
	 * those identifiers as referring to the same road. The resulting map will not provide any mapping of
	 * its milestones to its road identifier.
	 * 
	 * @param roadRefs
	 * @return A set of milestones on the requested road
	 */
	public NavigableMap<Distance, Set<Milestone>> get(Set<String> roadRefs) {
		if (roadRefs.size() == 0)
			return null;
		if (roadRefs.size() == 1)
			return get(roadRefs.iterator().next());
		NavigableMap<Distance, Set<Milestone>> res = new TreeMap<Distance, Set<Milestone>>();
		for (String roadRef : roadRefs) {
			NavigableMap<Distance, Set<Milestone>> curr = get(roadRef);
			if (curr == null)
				continue;
			for (Distance distance : curr.keySet())
				if (res.containsKey(distance))
					res.get(distance).addAll(curr.get(distance));
				else
					res.put(distance, curr.get(distance));
		}
		if (res.isEmpty())
			return null;
		return res;
	}

	/**
	 * Returns the milestones having the highest distance which is greater than, or equal to, the one supplied.
	 * 
	 * @param roadRef
	 * @param distance
	 * @return A set of milestones on the requested road, all at the same distance
	 */
	public Set<Milestone> getCeiling(String roadRef, Distance distance) {
		Set<Milestone> res = new HashSet<Milestone>();
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
		if (roadMilestones != null) {
			Distance key = roadMilestones.ceilingKey(distance);
			if (key != null)
				res.addAll(roadMilestones.get(key));
		}
		return res;
	}

	/**
	 * Returns the milestones which enclose the requested distance.
	 * 
	 * <p>The result is a map with up to two keys, one for the last distance marker before {@code distance}
	 * and one for the first distance marker after it. If the road has a distance marker located
	 * precisely at {@code distance}, it is never returned. If no matching distance markers are found on
	 * one side of {@code distance} (or both), the result may have one or zero entries.
	 * 
	 * @param roadRef
	 * @param distance
	 */
	public Map<Distance, Set<Milestone>> getEnclosing(String roadRef, Distance distance) {
		Map<Distance, Set<Milestone>> res = new HashMap<Distance, Set<Milestone>>();
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
		if (roadMilestones != null) {
			Distance floorKey = roadMilestones.floorKey(distance);
			Distance ceilingKey = roadMilestones.ceilingKey(distance);
			if (floorKey != null) {
				Set<Milestone> set = new HashSet<Milestone>();
				set.addAll(roadMilestones.get(floorKey));
				res.put(floorKey, set);
			}
			if (ceilingKey != null) {
				Set<Milestone> set = new HashSet<Milestone>();
				set.addAll(roadMilestones.get(ceilingKey));
				res.put(ceilingKey, set);
			}
		}
		return res;
	}

	/**
	 * Returns the milestones having the highest distance which is less than, or equal to, the one supplied.
	 * 
	 * @param roadRef
	 * @param distance
	 * @return A set of milestones on the requested road, all at the same distance
	 */
	public Set<Milestone> getFloor(String roadRef, Distance distance) {
		Set<Milestone> res = new HashSet<Milestone>();
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
		if (roadMilestones != null) {
			Distance key = roadMilestones.floorKey(distance);
			if (key != null)
				res.addAll(roadMilestones.get(key));
		}
		return res;
	}

	/**
	 * Returns the milestones having the highest distance which is strictly greater than the one supplied.
	 * 
	 * @param roadRef
	 * @param distance
	 * @return A set of milestones on the requested road, all at the same distance
	 */
	public Set<Milestone> getHigher(String roadRef, Distance distance) {
		Set<Milestone> res = new HashSet<Milestone>();
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
		if (roadMilestones != null) {
			Distance key = roadMilestones.higherKey(distance);
			if (key != null)
				res.addAll(roadMilestones.get(key));
		}
		return res;
	}

	/**
	 * Returns the milestones having the highest distance which is strictly less than the one supplied.
	 * 
	 * @param roadRef
	 * @param distance
	 * @return A set of milestones on the requested road, all at the same distance
	 */
	public Set<Milestone> getLower(String roadRef, Distance distance) {
		Set<Milestone> res = new HashSet<Milestone>();
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
		if (roadMilestones != null) {
			Distance key = roadMilestones.lowerKey(distance);
			if (key != null)
				res.addAll(roadMilestones.get(key));
		}
		return res;
	}

	/**
	 * Returns the milestones whose indicated distance is closest to the one supplied.
	 * 
	 * @param roadRef
	 * @param distance
	 * @return A set of milestones on the requested road, whose distances all differ from {@code distance} by the same value
	 */
	public Set<Milestone> getNearest(String roadRef, Distance distance) {
		Set<Milestone> res = new HashSet<Milestone>();
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(roadRef);
		if (roadMilestones != null) {
			if (roadMilestones.containsKey(distance))
				res.addAll(roadMilestones.get(distance));
			else {
				Distance floorKey = roadMilestones.floorKey(distance);
				Distance ceilingKey = roadMilestones.ceilingKey(distance);
				if ((floorKey == null) && (ceilingKey == null)) {
					// NOP
				} else if (floorKey == null)
					res.addAll(roadMilestones.get(ceilingKey));
				else if (ceilingKey == null)
					res.addAll(roadMilestones.get(floorKey));
				else {
					float floorDiff = distance.difference(floorKey, Distance.UNIT_M);
					float ceilingDiff = distance.difference(ceilingKey, Distance.UNIT_M);
					if (floorDiff < ceilingDiff)
						res.addAll(roadMilestones.get(floorKey));
					else if (ceilingDiff < floorDiff)
						res.addAll(roadMilestones.get(ceilingKey));
					else {
						/*
						 * FIXME for now add nothing, as the only consumer can work around an empty set
						 * but will choke on a set with multiple different distances.
						 * If needed, a better solution is to return a map (distance to set of milestones)
						 * and have the recipient discard maps with more than one entry.
						 */
					}
				}
			}
		}
		return res;
	}
}
