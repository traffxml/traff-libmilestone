/*
 * Copyright © 2020 traffxml.org.
 * 
 * This file is part of the traff-libmilestone library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lib.milestone;

import java.util.HashSet;
import java.util.Set;

import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.LatLon;

/**
 * Helper class for working with junctions.
 */
public class JunctionUtils {

	/**
	 * Merges a set of junctions into one.
	 * 
	 * <p>If {@code junctions} is null or empty, the result is null,
	 * 
	 * <p>If {@code junctions} has exactly one element, then that element will be returned.
	 * 
	 * <p>Otherwise, a new junction will be created according to the following rules:
	 * 
	 * <p>{@code geo} is the average of all individual coordinates (currently the center of the bounding box).
	 * 
	 * <p>For {@code roadRef}, {@code ref} and {@code name}, if all non-null values for each field are equal,
	 * then that value is used, else it is null (which also happens if all values are null).
	 * 
	 * <p>{@code distance} is the average of all non-null {@code distance} fields.
	 * 
	 * @param junctions The junctions to merge.
	 * @return The merged junction, or null if no junctions were supplied.
	 */
	public static Junction mergeJunctions(Set<Junction> junctions) {
		if ((junctions == null) || junctions.isEmpty())
			return null;
		if (junctions.size() == 1)
			return junctions.iterator().next();

		float kmSum = 0;
		float kmCount = 0;
		BoundingBox bbox = null;
		Set<String> refs = new HashSet<String>();
		Set<String> roadRefs = new HashSet<String>();
		Set<String> names = new HashSet<String>();
		for (Junction junction : junctions) {
			if (bbox == null)
				bbox = new BoundingBox(junction.geo);
			else
				bbox = bbox.extend(junction.geo);
			if (junction.roadRef != null)
				roadRefs.add(junction.roadRef);
			if (junction.ref != null)
				refs.add(junction.ref);
			if (junction.name != null)
				names.add(junction.name);
			if (junction.distance != null) {
				kmSum += junction.distance.asUnit(Distance.UNIT_KM);
				kmCount++;
			}
		}
		LatLon geo = null;
		if (bbox != null) {
			float lat = (bbox.minLat + bbox.maxLat) / 2.0f;
			float lon = (bbox.minLon + bbox.maxLon) / 2.0f;
			if (bbox.minLon > bbox.maxLon)
				lon -= 180.0f * Math.signum(lon);
			geo = new LatLon(lat, lon);
		}
		return new Junction(((roadRefs.size() == 1) ? roadRefs.iterator().next() : null),
				((refs.size() == 1) ? refs.iterator().next() : null),
				((names.size() == 1) ? names.iterator().next() : null),
				((kmCount > 0) ? new Distance(kmSum / kmCount, Distance.UNIT_KM) : null),
				geo);
	}

}
